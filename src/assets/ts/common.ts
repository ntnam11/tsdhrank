export interface ResultType {
  rank: number;
  rank_equal: number;
  total: number;
  area_rank: number;
  area_rank_equal: number;
  area_total: number;
}

export interface KeyValueString {
  key: string;
  value: string;
}

export function parseScore(score: string): number {
  let count = 0
  let index = -1
  const strScore = score
  for (let i = 0; i < strScore.length; i++)
  {
    const ord = strScore[i].charCodeAt(0)
    if (ord < 48 || ord > 57)
    {
      count++
      index = i
    }
  }
  if (count > 1)
  {
    return -1
  }
  let result = 0
  if (index != -1)
  {
    result = parseFloat(strScore.substring(0, index) + '.' + strScore.substring(index + 1, strScore.length))
  }
  else
  {
    result = parseFloat(strScore)
  }
  if (isNaN(result)) return -1
  return result
}