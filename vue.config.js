module.exports = {
    publicPath: process.env.NODE_ENV === "production" ? "/tsdhrank/" : "/",
    css: {
      loaderOptions: {
        sass: {
          prependData: `
          @import "~@/assets/css/base.scss";
          `
        }
      }
    },
    configureWebpack: {
      devtool: 'source-map'
    }
  };
  